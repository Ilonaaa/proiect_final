import React,{Component} from 'react'

class Playlist extends Component{
  constructor(props){
    super(props)
    this.state = {
      isEditing : false,
      playlist : this.props.playlist,
      playlistName : this.props.playlist.name,
      playlistDescription : this.props.playlist.description
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      playlist : nextProps,
      playlistName : this.props.playlist.name,
      playlistDescription : this.props.playlist.description,
      isEditing : false
    })
  }
  render(){
  if(this.state.isEditing){
      return (<div>
       Playlist: <input type="text" name="playlistName" value={this.state.playlistName} onChange={this.handleChange}/>
       Description:   <input type="text" name="playlistDescription" value={this.state.playlistDescription} onChange={this.handleChange}/> 
     
        <input type="button" value="save" onClick={() => this.props.onSave(this.props.playlist.id, {name : this.state.playlistName, description : this.state.playlistDescription})}/>
        <input type="button" value="cancel" onClick={() => this.setState({isEditing : false})} />
      </div>)            
    }
    else{
      return (<div>
        Playlist: <b>{this.state.playlistName}</b> 
             <input type="button" value="delete" onClick={() => this.props.onDelete(this.state.playlist.id)}/>
        <input type="button" value="edit" onClick={() => this.setState({isEditing : true})} />
        <input type="button" value="details" onClick={() => this.props.onSelect(this.props.playlist.id)}/> 
      </div>)
    }
  }
}

export default Playlist