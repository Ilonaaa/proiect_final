import React, {Component} from 'react'
import Video from './Video'
import VideoForm from './VideoForm'
import VideoStore from '../stores/VideoStore'
import {EventEmitter} from 'fbemitter'

const ee = new EventEmitter()
const store = new VideoStore(ee)

class PlaylistDetails extends Component{
  constructor(props){
    super(props)
    this.state = {
      videos : []
    }
    this.addVideo = (video) => {
      store.addOne(this.props.playlist.id, video)
    }
    this.deleteVideo = (video) => {
      store.deleteOne(this.props.playlist.id,video)
    }
    this.saveVideo = (video) => {
      store.saveOne(this.props.playlist.id,video)
      
    }
  }
  componentDidMount(){
    store.getAll(this.props.playlist.id)
    ee.addListener('VIDEO_LOAD', () => {
      this.setState({
        videos : store.content
      })
    })
  }
  render(){
    return (
      <div>
        <h2>PLaylist: <b>{this.props.playlist.name}</b> </h2>
        <h4>{this.props.playlist.description}</h4>
        <h3>List of videos from this playlist:</h3>
        {
          this.state.videos.map((p) => <Video video={p} onDelete={this.deleteVideo} key={p.id} onSave={this.saveVideo} />)
        }
        <h3>Add another one.</h3>
        <VideoForm onAdd={this.addVideo}/>
      </div>  
    )
  }
}

export default PlaylistDetails





