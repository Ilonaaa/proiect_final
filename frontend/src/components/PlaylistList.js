import React, {Component} from 'react'
import PlaylistStore from '../stores/PlaylistStore'
import {EventEmitter} from 'fbemitter'
import Playlist from './Playlist'
import PlaylistForm from './PlaylistForm'
import PlaylistDetails from './PlaylistDetails'

const ee = new EventEmitter()
const store = new PlaylistStore(ee)

function addPlaylist(playlist){
  store.addOne(playlist)
}

function deletePlaylist(id){
  store.deleteOne(id)
}

function savePlaylist(id, playlist){
  store.saveOne(id, playlist)
}

class PlaylistList extends Component{
  constructor(props){
    super(props)
    this.state = {
      playlists : [],
      detailsFor : -1,
      selected : null
    }
    this.cancelSelection = () => {
      this.setState({
        detailsFor : -1
      })
    }
    this.selectPLaylist = (id) => {
      store.getOne(id)
      ee.addListener('SINGLE_PLAYLIST_LOAD', () => {
        this.setState({
          detailsFor : store.selected.id,
          selected : store.selected
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('PLAYLIST_LOAD', () => {
      this.setState({
        playlists : store.content
      })
    })
  }
  render(){
    if (this.state.detailsFor === -1){
      return (<div>
        {
          this.state.playlists.map((c) => <Playlist playlist={c} onDelete={deletePlaylist} key={c.id} onSave={savePlaylist} onSelect={this.selectPLaylist} />)
        }
        <PlaylistForm handleAdd={addPlaylist}/>
      </div>)      
    }
    else{
      return (
        <div>
          <PlaylistDetails playlist={this.state.selected} />
          <input type="button" value="back" onClick={() => this.cancelSelection()}/>
        </div>  
      )
    }
  }
}

export default PlaylistList



