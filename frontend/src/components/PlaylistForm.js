import React,{Component} from 'react'

class PlaylistForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      playlistName : '',
      playlistDescription : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render(){
    return (<div>
      <h4>Playlists:</h4>
    
      Name : <input type="text" name="playlistName" onChange={this.handleChange}/>
      Description : <input type="text" name="playlistDescription" onChange={this.handleChange} />
      <input type="button" value="add" onClick={() => this.props.handleAdd({name : this.state.playlistName, description : this.state.playlistDescription})} />
    </div>)
  }
}

export default PlaylistForm