import React, { Component } from 'react'
import '../styles/App.css'
import PlaylistList from './PlaylistList'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to our playlist manager</h1>
        </header>
        <PlaylistList />
      </div>
    )
  }
}

export default App
