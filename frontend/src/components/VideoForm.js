import React,{Component} from 'react'

class VideoForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      videoTitle : '',
      videoChannel: '',
      videoUploadDate:'',
      videoYoutubeLink:''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }
  }
  render(){
    return (<div>
      Title : <input type="text" name="videoTitle" onChange={this.handleChange}/>
      Channel : <input type="text" name="videoChannel" onChange={this.handleChange} />
      UploadDate : <input type="text" name="videoUploadDate" onChange={this.handleChange} />
      Youtube Link : <input type="text" name="videoYoutubeLink" onChange={this.handleChange} />

      <input type="button" value="add" onClick={() => this.props.onAdd({title : this.state.videoTitle, channel:this.state.videoChannel, uploadDate:this.state.videoUploadDate, youtubeLink:this.state.videoYoutubeLink})} />
    </div>)
  }
}

export default VideoForm


