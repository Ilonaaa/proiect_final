import axios from 'axios'
const SERVER = 'https://project-ilonacojoaca.c9users.io:8081'

class VideoStore{
  constructor(ee){
    this.ee = ee
    this.content = []
  }
  getAll(playlistId){
    axios(SERVER + '/playlists/' + playlistId + '/videos')
      .then((response) => {
        this.content = response.data
        this.ee.emit('VIDEO_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(playlistId, video){
    axios.post(SERVER + '/playlists/' + playlistId + '/videos', video)
      .then(() => this.getAll(playlistId))
      .catch((error) => console.warn(error))
  }
  deleteOne(playlistId, videoId){
    axios.delete(SERVER + '/playlists/' + playlistId + '/videos/' + videoId)
      .then(() => this.getAll(playlistId))
      .catch((error) => console.warn(error))
  }
  saveOne(playlistId, videoId, video){
    axios.put(SERVER + '/playlists/' + playlistId + '/videos/' + videoId, video)
      .then(() => this.getAll(playlistId))
      .catch((error) => console.warn(error))
  }
}

export default VideoStore
