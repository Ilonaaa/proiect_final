import axios from 'axios'
const SERVER = 'https://project-ilonacojoaca.c9users.io:8081'

class PlaylistStore{
  constructor(ee){
    this.ee = ee
    this.content = []
    this.selected = null
  }
  getAll(){
    axios(SERVER + '/playlists')
      .then((response) => {
        this.content = response.data
        this.ee.emit('PLAYLIST_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(playlist){
    axios.post(SERVER + '/playlists', {...playlist,user_id:1})
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/playlists/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, playlist){
    axios.put(SERVER + '/playlists/' + id, playlist)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  getOne(id){
    axios(SERVER + '/playlists/' + id)
      .then((response) => {
        this.selected = response.data
        this.ee.emit('SINGLE_PLAYLIST_LOAD')
      })
      .catch((error) => console.warn(error))
  }
}

export default PlaylistStore
