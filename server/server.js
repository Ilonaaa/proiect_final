const express = require('express')
const bodyParser = require('body-parser')
const router = require('./routes')
const PORT = 8081;
const  Playlist = require('./models').Playlist;
const  Video = require('./models').Video;
const path = require("path")


const app = express()
app.use(bodyParser.json())

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../.', 'frontend/build', 'index.html'));
});


app.use(express.static(path.resolve(__dirname, '../frontend', 'build')));

app.use('/',router);



app.get('/playlists', (req, res, next) => {
    Playlist.findAll()
        .then((playlists) => res.status(200).json(playlists))
        .catch((error) => next(error))
})

app.post('/playlists', (req, res, next) => {
    Playlist.create({
        name: req.body.name,
        description: req.body.description,
        user_id:req.body.user_id
    })
        .then(() => res.status(201).send('created'))
        .catch((error,msg) => {next(error, msg)
        })
})

app.get('/playlists/:id', (req, res, next) => {
    Playlist.findById(req.params.id)
        .then((playlist) => {
            if (playlist){
                res.status(200).json(playlist)
            }
            else{
                res.status(404).send('not found')
            }
        })
        .catch((error) => next(error))
})

app.put('/playlists/:id', (req, res, next) => {
    Playlist.findById(req.params.id)
        .then((playlist) => {
            if (playlist){
                return playlist.update({
                    name: req.body.name,
                    description: req.body.description,
                })
            }
            else{
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headersSent){
                res.status(201).send('modified')
            }
        })
        .catch((error) => next(error))
})

app.delete('/playlists/:id', (req, res, next) => {
    Playlist.findById(req.params.id)
        .then((playlist) => {
            if (playlist){
                return playlist.destroy()
            }
            else{
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headersSent){
                res.status(201).send('modified')
            }
        })
        .catch((error) => next(error))
})

app.get('/playlists/:uid/videos', (req, res, next) => {
    Playlist.findById(req.params.uid)
        .then((playlist) => {
            if(playlist){
                return playlist.getVideos()
            }
            else{
                res.status(404).send('not found')
            }
        })
        .then((video) => {
            if (!res.headersSent){
                res.status(200).json(video)
            }
        })
        .catch((err) => next(err))
})

app.post('/playlists/:uid/videos', (req, res, next) => {
    Playlist.findById(req.params.uid)
        .then((playlist) => {
            if(playlist){
                let video = req.body
                video.playlist_id = playlist.id
                return Video.create(video)
            }
            else{
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headersSent){
                res.status(201).send('created')
            }
        })
        .catch((err) => next(err))
})

app.get('/playlists/:uid/videos/:vid', (req, res, next) => {
    Video.findById(req.params.vid)
        .then((video) => {
            if (video){
                res.status(200).json(video)
            }
            else{
                res.status(404).send('not found')
            }
        })
        .catch((err) => next(err))
})

app.put('/playlists/:uid/videos/:vid', (req, res, next) => {
    Video.findById(req.params.id)
        .then((video) => {
            if (video){
                return video.update(req.body)
            }
            else{
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headersSent){
                res.status(201).send('modified')
            }
        })
        .catch((err) => next(err))
})

app.delete('/playlists/:uid/videos/:vid', (req, res, next) => {
    Video.findById(req.params.id)
        .then((video) => {
            if (video){
                return video.destroy()
            }
            else{
                res.status(404).send('not found')
            }
        })
        .then(() => {
            if (!res.headersSent){
                res.status(201).send('removed')
            }
        })
        .catch((err) => next(err))
})

app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).send('some error...')
})


app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}.`);
});