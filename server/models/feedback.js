'use strict';



module.exports = function (sequelize, DataTypes) {
    return sequelize.define('feedback', {
        name: DataTypes.STRING,
        text: DataTypes.STRING
    }, {
        underscored: true
    });

};