'use strict';



module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user', {
    userName: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
  }, {
    underscored: true
  });

};