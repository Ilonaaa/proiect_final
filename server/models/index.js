'use strict';
const connection = require('../conf/db');
const User = connection.import('./user');
const Feedback = connection.import('./feedback');
const Video = connection.import('./video');
const Playlist = connection.import('./playlist');


User.hasMany(Playlist,{onDelete:'cascade'});

Playlist.hasMany(Video,{onDelete:'cascade'});

module.exports ={
  User,
  Video,
  Playlist,
  Feedback,
  connection
};
