# Manager videoclipuri favorite integrata cu youtube.
Tema proiectului 3.

## Descriere
Oamenii sunt direct influentati de muzica lor preferata, care le da o stare de bine, care ii calmeaza, unde isi gasesc refugiul in versuri care descriu exact starea pe care o au in multe momente, asadar, dependenta si placerea oamenilor pentru muzica nu este un lucru deloc strain pentru nimeni.

Asadar, aplicatia noastra are tocmai scopul de a pastra videoclipurile si melodiile care intr-un anumit moment iti trezesc o emotie, o amintire sau iti creeaza o stare de bine, pentru a putea fi reascultate si in alte momente fara a fi nevoie sa le accesezi de pe youtube, acestea putand fi gasite in playlist-urile din aplicatia noastra.

Functionalitatile de care aceasta va dispune nu se rezuma doar la vizualizarea playlist-urilor, ci si la adaugarea/stergea unui videoclip sau a unui playlist, mutarea videoclipurilor dintr-un playlist in altul, cat si ordonarea acestora, afisarea cronologica, in ordinea adaugarii lor. De asemenea, aplicatia va fi user-friendly, cu un meniu intuitiv si usor de folosit si va veni in ajutorul utilizatorilor ce isi doresc o organizare cat mai buna a videoclipurilor preferate si o accesare cat mai rapida a acestora.

## Functionalitati
- autentificare
- cautare dupa videoclipuri din youtube 
- vizualizarea, crearea, modificarea si stergerea playlist-urilor
- adaugare/stergere videoclipuri
- mutarea videoclipurilor dintr-un playlist in altul
- ordonarea cronologica a videoclipurilor
- cautarea unui videoclip pentru a fi adaugat in playlist



## Technologii
- Front-End: React + Material UI/Bootstrap
- Back-End: NodeJS + Express
- Baza de date: Sequelize + MySQL

## Baza de date:
![Baza de date](https://i.imgur.com/SklT7pC.png)